//program to implement LIRS-WSR algorithm for cache replacement policy for flash memory
// programmmed by: Dabbal Singh Mahara
//M.Sc. Roll.no: 11 (2011-13) batch Tribhuvan University, Kathmandu, Nepal

/* Input File: 
 *              trace file
 *              parameter file
 
 * Output File:  hit rate  it describes the hit rates for each 
                cache size specified in parameter file, pagefaults, write counts
              

/* Input File Format: 
 * (1) trace file: Block Number of each reference, which
     is the unique number for each accessed block together with integer 0 or 1 to represent access type 
 * (2) parameter file:    one or more cache sizes we want to test 
      
 */

#include "stdio.h"
#include "string.h"
#include "stdlib.h"


/* the size percentage of HIR blocks, default value is 1% of cache size */
#define HIR_RATE 1
#define LOWEST_HG_NUM 2

#define TRUE 1
#define FALSE 0

 struct node 
{
  char pn[9];
  char r;
  int cold;
  int isResident; 
  int isHIR_block;
  struct node * LIRS_next;
  struct node * LIRS_prev;
  struct node *HIR_rsd_next;
  struct node *HIR_rsd_prev;
  int    recency;
};

unsigned long total_pg_refs;
unsigned long distinct_refs; /* counter excluding duplicate refs */
unsigned long num_pg_flt;
unsigned long write_request;
unsigned long write_count;
unsigned long free_mem_size, mem_size;

struct node * LIRS_stack_head;
struct node * LIRS_stack_tail;
struct node * HIR_list_head;
struct node * HIR_list_tail;

unsigned long HIR_block_portion_limit;
FILE *trace_fp, *out_1_fp, *para_fp;

char history[200000][9];

 void add_LIRS_stack_head(struct node *new_ref_ptr);
 void move_to_LIRS_stack_head(struct node *new_ref_ptr);
 int add_to_HIR_List(struct node * new_HIR_ptr);
 int remove_from_HIR_List(struct node *HIR_block_ptr);
 void move_to_head_HIR_List(struct node * new_HIR_ptr );
 int remove_HIR_tail();
 FILE *openReadFile(char file_name[]);
 void LIRSWSR();
 void stack_prune(); 
 
 
 
//start of main
 
 int main()
{
  
  int i,j;
  char trc_file_name[100]; 
  char para_file_name[100];
  char out_file_name[100];
   
  printf("Enter parameter file name\n");
  gets(para_file_name);
  strcat(para_file_name, ".par");
  para_fp = openReadFile(para_file_name);
  
  printf("\n Enter trace filename\t");
  gets(trc_file_name);
  strcat(trc_file_name, ".trace");
  trace_fp = openReadFile(trc_file_name);
  
  printf("\n Enter output filename");
  gets(out_file_name);
  strcat(out_file_name, ".txt");
  out_1_fp = fopen(out_file_name, "w");
  
  fprintf(out_1_fp,"OUTPUT OF LIRS-WSR for flash ALGORITHM\n\n\n\n");
  
   fprintf(out_1_fp,"Memory size Total #References MISSRate  HIT Rate #Page faults #Distict_references HIR  Limit #Write Count #Write Requests ");
  
     /* Actually read the size of whole reference space and trace length */ 
  fscanf(para_fp, "%lu", &mem_size);
 
  

  while (!feof(para_fp))
  {
  
        /* the memory ratio for hirs is 1% */
        HIR_block_portion_limit = (unsigned long)(HIR_RATE/100.0*mem_size); 
        if (HIR_block_portion_limit < LOWEST_HG_NUM)
        HIR_block_portion_limit = LOWEST_HG_NUM;

        printf(" \nLhirs (cache size for HIR blocks) = %lu\n", HIR_block_portion_limit);
        printf("\nmem_size = %lu\n", mem_size);
       
       
        distinct_refs = 0;
        num_pg_flt = 0;
        write_request=0;
        write_count =0;
        total_pg_refs=0;
        free_mem_size = mem_size;
        
        LIRS_stack_head=NULL;
        LIRS_stack_tail=NULL;
        HIR_list_head=NULL;
        HIR_list_tail=NULL;
       
       
      for(i=0;i<200000;i++)
       {
         for(j=0;j<9;j++)
           history[i][j]='\0';
           
        }  
       
      
      // calling LIRS replacement algorithm
       LIRSWSR();
       
       printf("\n Total references = %lu\n Total distinct references = %lu\n Total page faults = %lu \n Total write count = %lu\n Total Write requests = %lu",total_pg_refs,distinct_refs,num_pg_flt,write_count,write_request);
       printf("\ntotal blocks refs = %lu \n number of misses(page faults) = %lu \n miss rate = %2.1f\% \n", total_pg_refs, num_pg_flt,(float)(num_pg_flt-distinct_refs)/(total_pg_refs-distinct_refs)*100);
      
       fprintf(out_1_fp, "\n%5lu \t%lu\t %2.1f\t%2.1f\t%d\t%lu\t\t%lu\t\t%2.2f\t%lu\t%lu", mem_size,total_pg_refs, ((float)num_pg_flt-distinct_refs)/(total_pg_refs-distinct_refs)*100,100-((float)num_pg_flt-distinct_refs)/(total_pg_refs-distinct_refs)*100,num_pg_flt,distinct_refs,HIR_block_portion_limit,(float)write_count/write_request*100,write_count,write_request);
 
    fscanf(para_fp, "%lu", &mem_size);
    
    } //while close
    
  
  fclose(out_1_fp);
  fclose(trace_fp);
  fclose(para_fp);
  system("pause");
  return 0;
} // end of the main
 
  
FILE *openReadFile(char file_name[])
{
  FILE *fp;
  fp = fopen(file_name, "r");
  if (fp==NULL) {
    printf("can not find file %s.\n", file_name);
    return NULL;
  }
 return fp;
}

//LIRS WSR algorithm
void LIRSWSR()
{
   char ref_block[9],s[9];
   char r;
   int i,j;
   int flag;
   
   struct node *temp,*newnode;
   struct node *hold;
   struct node *temp1;
    
   fseek(trace_fp,0, SEEK_SET); 
 
   fscanf(trace_fp,"%s",s);
  
   while (!feof(trace_fp))
   {
      
     
      for(i=0;i<9;i++)
        ref_block[i] = '\0';
        
      if(s[0]=='1')
         r='W';
       else
         r='R';
      for(i=2,j=0;s[i]!='\0';i++,j++)
         ref_block[j] = s[i];
      ref_block[j] = '\0';    
              
      flag =0;
        
      total_pg_refs++;
      
      for(i=1;i<=distinct_refs;i++)
       {

          if(strcmp(history[i],ref_block)==0)
            {
                           
               flag =1;
               break;
            } 
       }  
      if(flag == 0)
       {
              distinct_refs++;
              printf("\n %lu",distinct_refs);
              strcpy(history[distinct_refs],ref_block);       
       }       
      if(r=='W')
      {
        write_request++;
      }
          
     // isin cache
     // Search for hit in LIRS Stack
     temp= LIRS_stack_head;
     while(temp!=NULL)
     {
       flag =0;              
      if(strcmp(ref_block,temp->pn)==0)
       {
         if(temp->isHIR_block==0)
         {
                
                                     
            if(r=='W')
               temp->r='W';
            
               
             move_to_LIRS_stack_head(temp);
             temp->cold =0;
             if(temp->LIRS_next == NULL)
                  stack_prune();
                  
              // printf("\n hit in LIR....%s",ref_block);
                                  
           }
          else if(temp->isHIR_block==1)
            {
               if(temp->isResident ==1) 
               {
                  // printf("\n hit in HIR....%s\t%d",temp->pn,temp->isHIR_block );      
                     if(r=='W')
                         temp->r='W';
                       temp->cold =0;
                       move_to_LIRS_stack_head(temp);
                       remove_from_HIR_List(temp); 
                     
                       while(LIRS_stack_tail->r=='W' && LIRS_stack_tail->cold==0)
                       {
                            move_to_LIRS_stack_head(LIRS_stack_tail);
                            LIRS_stack_head->cold=1;
                            stack_prune();
                       }
                                            //clean or cold and dirty page
                    add_to_HIR_List(LIRS_stack_tail);
                    LIRS_stack_tail=LIRS_stack_tail->LIRS_prev;
                    LIRS_stack_tail-> LIRS_next->LIRS_prev = NULL;
                    LIRS_stack_tail->LIRS_next=NULL;
                    stack_prune();                
                 }
                 else
                 {
                     num_pg_flt++; 
                    if(r=='W')
                        temp->r='W';
                     else 
                        temp->r='R';
                    remove_HIR_tail(); 
                        
                     move_to_LIRS_stack_head(temp);
                     temp->cold=0;
                        
                 while(LIRS_stack_tail->r=='W' && LIRS_stack_tail->cold==0)
                 {
                    move_to_LIRS_stack_head(LIRS_stack_tail);
                    LIRS_stack_head->cold=1;
                    stack_prune();
                 }
                  add_to_HIR_List(LIRS_stack_tail); // move stack bottom to HIR list head
                  LIRS_stack_tail=LIRS_stack_tail->LIRS_prev;
                  LIRS_stack_tail->LIRS_next->LIRS_prev = NULL;
                  LIRS_stack_tail->LIRS_next=NULL;
                  stack_prune();
                 }
             } 
           
           flag =1;
           break;     
       }
       temp=temp->LIRS_next;
     }
      if(flag == 1)
       {
                  fscanf(trace_fp,"%s",s);
                 continue;
        }
      else // Not in stack
      {
                              
      // Search for hit in HIR Q  
       temp = HIR_list_head;
        while(temp!= NULL) // hit in cache
        {
          flag=0;   
          if(strcmp(ref_block,temp->pn)==0)
            { 
                if(r=='W')
                   temp->r='W';    
                move_to_head_HIR_List(temp);
                add_LIRS_stack_head(temp);
                flag=1;
                break;
            }   
            
            temp= temp->HIR_rsd_next;
        }
      }  
      if(flag == 1)
       {
                  fscanf(trace_fp,"%s",s);
                 continue;
       } 
       else
       { 
           
         // printf("\ngenerates block miss */");
           num_pg_flt++;
           if (free_mem_size == 0)
           {
                   remove_HIR_tail();
                  
                     newnode = (struct node *) malloc(sizeof(struct node));
                     newnode->isHIR_block = 1;
                     newnode->isResident  = 1;
                     strcpy(newnode->pn,ref_block);
                     newnode->recency = 1;
                     newnode->LIRS_next = NULL;
                     newnode->LIRS_prev = NULL;          
                     newnode->HIR_rsd_next = NULL;
                     newnode->HIR_rsd_prev  = NULL;
                     if(r=='W')
                       newnode->r='W';
                     else 
                         newnode->r='R';
                    
                   if(newnode->r=='W')
                    {
                        add_LIRS_stack_head(newnode);
                        newnode->isHIR_block = 0;
                        
                         while(LIRS_stack_tail->r=='W' && LIRS_stack_tail->cold==0)
                          {
                              move_to_LIRS_stack_head(LIRS_stack_tail);
                              LIRS_stack_head->cold=1;
                              stack_prune();
                           }
                       add_to_HIR_List(LIRS_stack_tail); // move stack bottom to HIR list head
                       LIRS_stack_tail=LIRS_stack_tail->LIRS_prev;
                       LIRS_stack_tail->LIRS_next->LIRS_prev = NULL;
                       LIRS_stack_tail->LIRS_next=NULL;
                       stack_prune(); 
                    }
                    else
                    {
                       add_to_HIR_List(newnode);
                       add_LIRS_stack_head(newnode);
                    }  
                    
                    fscanf(trace_fp,"%s",s);
                     continue; 
                }
               
         else if( free_mem_size> HIR_block_portion_limit) //to place page in LIR block
            {
                newnode = (struct node *) malloc(sizeof(struct node));
                newnode->isHIR_block = 0;
                newnode->isResident  = 0;
                strcpy(newnode->pn,ref_block);
                newnode->recency = 1;
                newnode->LIRS_next = NULL;
                newnode->LIRS_prev = NULL;          
                newnode->HIR_rsd_next = NULL;
                newnode->HIR_rsd_prev  = NULL;
                if(r=='W')
                        newnode->r='W';
                else 
                        newnode->r='R';
                
                add_LIRS_stack_head(newnode);
                free_mem_size--;
                fscanf(trace_fp,"%s",s);
                continue; 
                
          }
          else    //to place page in HIR block
           {
               
                newnode = (struct node *) malloc(sizeof(struct node));
                newnode->isHIR_block = 1;
                newnode->recency = 1;
                newnode->isResident = 1;
                strcpy(newnode->pn,ref_block);
                newnode->LIRS_next = NULL;
                newnode->LIRS_prev = NULL;          
                newnode->HIR_rsd_next = NULL;                   
                newnode->HIR_rsd_prev  = NULL;
                if(r=='W')
                        newnode->r='W';
                else 
                        newnode->r='R';
                        
               if(newnode->r=='W')
                    {
                      add_LIRS_stack_head(newnode);
                      newnode->isHIR_block = 0;
                        
                      while(LIRS_stack_tail->r=='W' && LIRS_stack_tail->cold==0)
                          {
                              move_to_LIRS_stack_head(LIRS_stack_tail);
                              LIRS_stack_head->cold=1;
                              stack_prune();
                           }
                       add_to_HIR_List(LIRS_stack_tail); // move stack bottom to HIR list head
                       LIRS_stack_tail=LIRS_stack_tail->LIRS_prev;
                       LIRS_stack_tail->LIRS_next->LIRS_prev = NULL;
                       LIRS_stack_tail->LIRS_next=NULL;
                       stack_prune(); 
                       free_mem_size--;
                    }
                 else
                 {   
                     add_LIRS_stack_head(newnode);
                     add_to_HIR_List(newnode);
                     free_mem_size--;
                 }   
               
                 fscanf(trace_fp,"%s",s);
                continue; 
                
             } 
          } //else close    
               
       } //while closed
        
    hold = LIRS_stack_head;
   while(hold!=NULL)
  {
     
     if(hold->r=='W')
        {
           write_count++;
         }
       hold=hold->LIRS_next;  
   }                  
   
   
   hold = HIR_list_head;
   while(hold!=NULL)
  {
     if(hold->recency== 0 && hold->r=='W')
        {
           write_count++;
         }
       hold=hold->HIR_rsd_next;  
   }                
   
   
}

//moves a node to top of stack from stack itself
void move_to_LIRS_stack_head(struct node *new_ref_ptr)
{
   
   if(new_ref_ptr->isHIR_block==1)
          new_ref_ptr->isHIR_block=0;   
   if( new_ref_ptr==LIRS_stack_head)
   {
     return;
    }
  else
  {  
            
    new_ref_ptr->LIRS_prev->LIRS_next =new_ref_ptr->LIRS_next;
    if(new_ref_ptr->LIRS_next == NULL)
            LIRS_stack_tail = LIRS_stack_tail->LIRS_prev;
    else
            new_ref_ptr->LIRS_next->LIRS_prev = new_ref_ptr->LIRS_prev;
       
    new_ref_ptr->LIRS_next =LIRS_stack_head; 
    LIRS_stack_head->LIRS_prev=new_ref_ptr;
    new_ref_ptr->LIRS_prev = NULL;
    LIRS_stack_head= new_ref_ptr;
      return;
  }
     
 }



// stack prunning
void stack_prune()
{
    if(LIRS_stack_tail->isHIR_block==0)
          return;
     else
     {
         LIRS_stack_tail->recency = 0;
         LIRS_stack_tail = LIRS_stack_tail->LIRS_prev;
         LIRS_stack_tail->LIRS_next->LIRS_prev =NULL;
         LIRS_stack_tail->LIRS_next=NULL;
         
         stack_prune();
     }   
}



/* Add a node to head of LIRS stack */
void add_LIRS_stack_head(struct node *new_ref_ptr)
{
     struct node *temp;
  if(new_ref_ptr->recency==0)
       new_ref_ptr->recency =1;

  if (LIRS_stack_head==NULL){
      LIRS_stack_head = LIRS_stack_tail = new_ref_ptr;
     } 
  else 
  {
   LIRS_stack_head->LIRS_prev = new_ref_ptr;
   new_ref_ptr->LIRS_next =LIRS_stack_head; 
   LIRS_stack_head = new_ref_ptr;
  }
   
  return;
}

// removes a node from HIR list
 int remove_from_HIR_List(struct node *HIR_block_ptr)
{
   HIR_block_ptr->isResident=0;
   if(HIR_list_head == HIR_list_tail)
     {
          HIR_list_head =HIR_list_tail = NULL;
          return TRUE;
     }
    else if (HIR_block_ptr->HIR_rsd_prev==NULL) // first node
    {
       HIR_list_head = HIR_block_ptr->HIR_rsd_next;
       HIR_list_head->HIR_rsd_prev = NULL;
       HIR_block_ptr->HIR_rsd_next = NULL;
    }
    else if(HIR_block_ptr->HIR_rsd_next == NULL) //last node
     {
       HIR_list_tail= HIR_block_ptr->HIR_rsd_prev;
       HIR_list_tail->HIR_rsd_next = NULL;
       HIR_block_ptr->HIR_rsd_prev = NULL;
     }
   else // node in between
    {
     HIR_block_ptr->HIR_rsd_prev->HIR_rsd_next = HIR_block_ptr->HIR_rsd_next;
     HIR_block_ptr->HIR_rsd_next->HIR_rsd_prev = HIR_block_ptr->HIR_rsd_prev;
     HIR_block_ptr->HIR_rsd_next = NULL;
     HIR_block_ptr->HIR_rsd_prev = NULL;
   }  
  return TRUE;
     
 }
 
 
 
 //Adding to the head of HIR list
 int add_to_HIR_List(struct node * new_HIR_ptr)
 {
      if(new_HIR_ptr->isHIR_block==0)
      {
           new_HIR_ptr->isHIR_block=1;    // This stack bottom is demoted to HI 
           new_HIR_ptr->recency = 0;     // removes from stack
                 
      }    
        new_HIR_ptr->isResident =1;
       if (HIR_list_head==NULL)
          HIR_list_tail = HIR_list_head = new_HIR_ptr;
       else
       {
         HIR_list_head->HIR_rsd_prev = new_HIR_ptr;
         new_HIR_ptr->HIR_rsd_next = HIR_list_head;
         new_HIR_ptr->HIR_rsd_prev= NULL;
         HIR_list_head = new_HIR_ptr;
       }
  return TRUE;
                         
}

// moves a node from HIR list to its head
void move_to_head_HIR_List(struct node * new_HIR_ptr )
{
     if(new_HIR_ptr==HIR_list_head)
           return;
     else 
     {
         new_HIR_ptr->HIR_rsd_prev->HIR_rsd_next = new_HIR_ptr->HIR_rsd_next;
         
         if(new_HIR_ptr->HIR_rsd_next==NULL)
              HIR_list_tail=HIR_list_tail->HIR_rsd_prev;
          else
                     new_HIR_ptr->HIR_rsd_next->HIR_rsd_prev = new_HIR_ptr->HIR_rsd_prev;
          
          new_HIR_ptr->HIR_rsd_next =HIR_list_head; 
          new_HIR_ptr->HIR_rsd_prev = NULL;
          HIR_list_head->HIR_rsd_prev = new_HIR_ptr;
          HIR_list_head = new_HIR_ptr;
      }
}

// remove HIR Tail node
int remove_HIR_tail()
{
   // printf("\n remove tail");
    HIR_list_tail->isResident=0; 
    if(HIR_list_tail->r=='W')
    {
         HIR_list_tail->r='R';
         write_count++;
    }    
    HIR_list_tail = HIR_list_tail->HIR_rsd_prev; 
    
    if (HIR_list_tail==NULL) // single node case
              HIR_list_head = NULL;
     else
     {
       HIR_list_tail->HIR_rsd_next->HIR_rsd_prev = NULL;
       HIR_list_tail->HIR_rsd_next = NULL;
        
     }
   return TRUE;
}
